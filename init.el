
;;lancement dufichier de chargement des configurations

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;(package-initialize)


;; Load all the configuration
(load-file "~/.emacs.d/configuration/header.el")




;;modifications made by customize.el
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(TeX-source-correlate-method (quote synctex))
 '(TeX-source-correlate-mode t)
 '(TeX-source-correlate-start-server t)
 '(ac-modes
   (quote
    (emacs-lisp-mode lisp-mode lisp-interaction-mode slime-repl-mode nim-mode c-mode cc-mode c++-mode go-mode java-mode malabar-mode clojure-mode clojurescript-mode scala-mode scheme-mode ocaml-mode tuareg-mode coq-mode haskell-mode agda-mode agda2-mode perl-mode cperl-mode python-mode ruby-mode lua-mode tcl-mode ecmascript-mode javascript-mode js-mode js-jsx-mode js2-mode js2-jsx-mode php-mode css-mode scss-mode less-css-mode makefile-mode sh-mode fortran-mode f90-mode ada-mode xml-mode sgml-mode web-mode ts-mode sclang-mode verilog-mode qml-mode apples-mode pascal-mode latex-mode html-mode sql-mode)))
 '(auto-package-update-delete-old-versions t)
 '(browse-url-browser-function (quote xwidget-webkit-browse-url))
 '(compilation-ask-about-save nil)
 '(cua-mode t nil (cua-base))
 '(highlight-symbol-idle-delay 0.5)
 '(ido-mode (quote buffer) nil (ido))
 '(initial-buffer-choice "~/Documents")
 '(initial-frame-alist (quote ((fullscreen . maximized))))
 '(initial-scratch-message ";; Notes

")
 '(limit-size-pdf 4000000)
 '(package-selected-packages
   (quote
    (i3wm helm-make evil-numbers helm-youtube helm-projectile projectile use-package ac-js2 ac-emmet ac-php emmet-mode web-mode firefox-controller ac-html 2048-game color-theme-tango windata undo-tree tablist tabbar switch-window spinner smex seq rainbow-delimiters powerline pdf-tools neotree multiple-cursors mode-compile markdown-mode magit indent-guide imenu-anywhere ido-yes-or-no hydra highlight-symbol helm dired-single commenter color-theme autopair auto-package-update auto-complete-auctex auctex alpha aggressive-indent ace-jump-mode ac-math)))
 '(pascal-ident-level 4)
 '(tabbar-cycle-scope (quote groups))
 '(tabbar-mode t nil (tabbar))
 '(tabbar-mwheel-mode t nil (tabbar))
 '(web-mode-block-padding 2)
 '(web-mode-enable-current-element-highlight t))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(hl-line ((t (:background "#3F3F3F"))))
 '(rainbow-delimiters-depth-1-face ((t (:foreground "white"))))
 '(rainbow-delimiters-depth-2-face ((t (:foreground "blue"))))
 '(rainbow-delimiters-depth-3-face ((t (:foreground "red"))))
 '(rainbow-delimiters-depth-4-face ((t (:foreground "green"))))
 '(rainbow-delimiters-depth-5-face ((t (:foreground "orange"))))
 '(rainbow-delimiters-depth-6-face ((t (:foreground "magenta"))))
 '(rainbow-delimiters-depth-7-face ((t (:foreground "sienna"))))
 '(rainbow-delimiters-depth-8-face ((t (:foreground "olive drab"))))
 '(rainbow-delimiters-depth-9-face ((t (:foreground "VioletRed1"))))
 '(rainbow-delimiters-unmatched-face ((t (:foreground "black")))))

(put 'scroll-left 'disabled nil)
