;;Add official repositories

(require 'package)

(add-to-list 'package-archives
	      '("melpa" . "http://melpa.org/packages/")
	      t)


(add-to-list 'package-archives
	      '("marmalade" . "https://marmalade-repo.org/packages/")
	      t)

(add-to-list 'package-archives
	     '("gnu" . "http://elpa.gnu.org/packages/")
	      t)



(package-initialize)

;; Inspired by : http://batsov.com/articles/2012/02/19/package-management-in-emacs-the-good-the-bad-and-the-ugly/
;; Package i3wm : integration with the workspace manager i3-wm. Delete it if you don't want to use i3

(defvar prelude-packages
  '(	   auto-package-update color-theme use-package request i3wm	; Sys
	   autopair evil-numbers firefox-controller magit highlight-symbol indent-guide ido-yes-or-no imenu-anywhere multiple-cursors neotree pdf-tools powerline rainbow-delimiters smex yasnippet undo-tree tabbar alpha ; Features
	   doctags projectile  				     ; C
	   emmet-mode js2-mode	php-mode skewer-mode web-mode		     ; Web
	   markdown-mode				     ; Markdown
	   arduino-mode					     ; Arduino
	   auctex lorem-ipsum ; Latex
	   helm helm-projectile helm-make		; Helm
	   auto-complete ac-emmet ac-html ac-js2 ac-math ac-php auto-complete-auctex math-symbol-lists ; Autocomplete
	   )
  "A list of packages to ensure are installed at launch.")

(defvar prelude-init t
  "If nil then prelude search for packages from `prelude-packages' to install.

Initialize by `prelude-packages-need-install'")

(defun prelude-packages-need-install ()
  "Determine if all packages from `prelude-packages' are installed.

If not, set `prelude-init' to nil"
  (dolist (p prelude-packages)
    (unless (memq p package-activated-list)
      (setq prelude-init nil)
      ))
  )

(defun prelude-packages-install ()
  ;; check for new packages
  (unless prelude-init 
    (message "%s" "Emacs Prelude is now refreshing its package database...")
    (package-refresh-contents)
    (message "%s" " done.")
    ;; install the missing packages
    (dolist (p prelude-packages)
      (when (not (package-installed-p p))
	(package-install p)))
    )
  )

(prelude-packages-need-install)
(prelude-packages-install)
