;; Dired configuration

(add-hook 'dired-mode-hook
	  (lambda ()
	    (local-set-key (kbd "<mouse-2>") 'dired-find-file) ; Dired always open files and folders in the same window
	    )
	  )
