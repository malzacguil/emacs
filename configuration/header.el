;; Load all packages and repositories
(load-file "~/.emacs.d/configuration/conf-packages.el")

;; Load proxy
;(load-file "~/.emacs.d/configuration/proxy.el") ; Proxy salle CPI EISTI Pau

;; Load gobal packages
(load-file "~/.emacs.d/configuration/global-packages.el")

;; Load snippet
(load-file "~/.emacs.d/configuration/conf-snippet.el")

;; Interface customizations
(load-file "~/.emacs.d/configuration/interface.el")

;; Modify global map key
(load-file "~/.emacs.d/configuration/map-key.el")

;; Load global configuration for all the programation modes
(load-file "~/.emacs.d/configuration/conf-prog-mode.el")

;; Load specifics languages configurations
;; (load-file "~/.emacs.d/configuration/prog-mode/ocaml.el") ;; Require opam, tuareg, merlin, ocp...
(load-file "~/.emacs.d/configuration/prog-mode/pascal.el")
(load-file "~/.emacs.d/configuration/prog-mode/arduino.el")
(load-file "~/.emacs.d/configuration/prog-mode/latex.el")
(load-file "~/.emacs.d/configuration/prog-mode/markdown.el")
(load-file "~/.emacs.d/configuration/prog-mode/web.el")
;(load-file "~/.emacs.d/configuration/prog-mode/c.el")
;; (load-file "~/.emacs.d/configuration/prog-mode/css.el") ;; Not working
;(load-file "~/.emacs.d/configuration/prog-mode/php.el") ; Need this ?? FIX IT

;; Load all over modes
(load-file "~/.emacs.d/configuration/conf-dired.el") ; Navigate throught folders
(load-file "~/.emacs.d/configuration/conf-neotree.el") ; Display a directory
(load-file "~/.emacs.d/configuration/conf-xwidget.el") ; Integrate navigator
(load-file "~/.emacs.d/configuration/conf-pdf-tool.el") ; Display pdf in emacs (better than the built-in module)
;(load-file "~/.emacs.d/configuration/helm-youtube.el") ; Youtube intégration
;(load-file "~/.emacs.d/configuration/conf-emms.el") ; Music player, not working
