;; Add snippet directory
(setq yas-snippet-dirs
      '("~/.emacs.d/configuration/snippets"))
;; Load snippets
(require 'yasnippet)
(yas-global-mode 1)
