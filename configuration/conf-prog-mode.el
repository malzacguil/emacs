;; Default autocompletion profile for all programming mode. See ac-mode variable for a list programming mode
(defun my-ac-prog-mode ()
  (setq ac-sources
	(append '(ac-source-yasnippet)
		ac-sources)))

(add-hook 'prog-mode-hook
	  (lambda ()
	    (linum-on) ; Make sure line numbers are activate 
	    (highlight-symbol-mode t) ; Highlight similar words
	    (rainbow-delimiters-mode) ; Rainbow parenthesis
	    (my-ac-prog-mode) ; Activate default autocompetion profile
	    (local-set-key (kbd "C-x m") 'helm-imenu) ; Helm feature : allow to search functions and variables declaration in a buffer 
	    (local-set-key (kbd "C-x ù") 'helm-imenu-anywhere) ; Helm feature : allow to search functions and variables declaration in all buffer with the same language
	    (local-set-key (kbd "M-m") 'helm-make)	       ; Helm feature : execute a makefile (locate in the current directory) in an helm buffer (choose a target)
	    )
	  t)
