;; Undo/redo tree
(global-undo-tree-mode)


;; Autocompletion
(require 'auto-complete-config)
(global-auto-complete-mode t)
(ac-config-default)


;; Autopair char like ( " [ {
(autopair-global-mode)


;; Use Ido to answer yes-or-no questions 
(ido-yes-or-no-mode)


;; Show open and close parenthesis 
(show-paren-mode)


;; Desactivate auto save
(setq auto-save-default nil)


;; Default style for commenting code
(setq comment-style 'extra-line)


;; Programme un dictonary use for automatic correction
(setq ispell-program-name "aspell") 
(setq ispell-dictionary "french")


;; Desactivate message when closing emacs with asynchronisous jobs are working
(add-hook 'comint-exec-hook 
	  (lambda () (set-process-query-on-exit-flag (get-buffer-process (current-buffer)) nil)))


;; Replace docview by pdf-tool
(pdf-tools-install)

;; Transparency
(require 'alpha)
