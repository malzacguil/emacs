;; theme
(load-theme 'wombat)


;; ALWAYS UTF8
(prefer-coding-system 'utf-8)


;; Frame name display on the top of emacs frame. You can change it for (buffer-name)
(setq frame-title-format "bachi-bouzouk")


;; Diseable menu bar 
(menu-bar-mode -1)


;; Diseable tool bar
(tool-bar-mode -1)


;; Diseable sroll bar
(scroll-bar-mode -1)


;; smooth scroll. Can create lag on slow computer
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq scroll-step 1) ;; keyboard scroll one line at a time


;; Font default size
(set-face-attribute 'default nil :height 100)


;; Display a bar on the top of emacs with tab group by languages
(tabbar-mode)


;; Display line nummber on the left and column number on the bottom right
(global-linum-mode)
(column-number-mode)


;; Change cursor type. Try bar or vbar if you have some issues to find your cursor
(set-default 'cursor-type 'hbar)


;; More beautiful inferior bar 
(powerline-center-theme)
(setq powerline-default-separator 'wave)


;; Display indentation guide. Could provoke some display bugs. Simply diseabled it to solve the problem
(indent-guide-global-mode)


;; Underligne the current line.
(global-hl-line-mode)

;; Set intial transparency value
(transparency-set-value 94)

;; Don't cut words at the end of a line
(setq truncate-partial-widht-windows nil)
