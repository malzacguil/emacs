;; Add file extention to web mode
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))




;; highlight html tags
(setq web-mode-enable-current-element-highlight t)



;; Create my aown profile for autocompletion in web-mode
(defun setup-ac-for-html ()
  ;; Require ac-haml since we are setup haml auto completion
  (require 'ac-html)
  ;; Require default data provider if you want to use
  (require 'ac-html-default-data-provider)
  ;; Enable data providers,
  ;; currently only default data provider available
  (ac-html-enable-data-provider 'ac-html-default-data-provider)
  ;; Let ac-haml do some setup
  (ac-html-setup))

(require 'ac-php)
;; Set up completion for each code sources
(setq web-mode-ac-sources-alist
  '(("php" . (ac-source-yasnippet ac-source-php))
    ("html" . (ac-source-yasnippet ac-source-emmet-html-aliases ac-source-emmet-html-snippets ac-source-html-tag ac-source-html-attr ac-source-html-attrv))
    ("css" . (ac-source-yasnippet ac-source-emmet-css-snippets))))

;; Add snippet for each code sources
(add-hook 'web-mode-before-auto-complete-hooks
          '(lambda ()
             (let ((web-mode-cur-language
                    (web-mode-language-at-pos)))
               (if (string= web-mode-cur-language "php")
                   (yas-activate-extra-mode 'php-mode)
                 (yas-deactivate-extra-mode 'php-mode))
               (if (string= web-mode-cur-language "css")
                   (progn
		     (setq emmet-use-css-transform t)
		     (yas-activate-extra-mode 'css-mode))
                 (setq emmet-use-css-transform nil)
		 (yas-deactivate-extra-mode 'css-mode))
	       (if (string= web-mode-cur-language "html")
		     (yas-activate-extra-mode 'html-mode)
		 (yas-deactivate-extra-mode 'html-mode)))))




;; Set up web mode
(add-hook 'web-mode-hook
	  '(lambda ()
	     (emmet-mode)		; Activate emmet
	     (ac-emmet-html-setup)	; add autocompletion for emmet
	     (setup-ac-for-html)	; Add my profile for auto-complete
	     (skewer-html-mode)		; Activate skewer
	     ))
