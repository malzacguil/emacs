;; Make changes in css mode
(add-hook css-mode-hook
	  '(lambda
	     (emmet-mode)		; Add emmet mode
	     (skewer-css-mode)		; load skewer
	     ))
