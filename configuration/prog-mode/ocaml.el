;; Add opam emacs directory to the load-path
(load "~/.opam/system/share/emacs/site-lisp/tuareg-site-file")
(setq opam-share (concat (shell-command-to-string "opam config var share 2> /dev/null")))
(add-to-list 'load-path (concat opam-share "/emacs/site-lisp"))

;; Load merlin-mode
(require 'merlin)
(setq merlin-ac-setup t);;merlin with autocomplete

;;load indentation for ocaml
(add-to-list 'load-path "~/.opam/system/share/emacs/site-lisp")
(require 'ocp-indent)

;; Load ocaml-mode (name tuareg-mode)
(require 'tuareg)

;; Module witch add some features to the ocaml toplevel and permit to automatic execute a file in it

(defcustom percentage-window-ocaml-toplevel 0.25
  "Percentage of the current window maximum size (between 0 and 1) reserved for the ocaml toplevel interface"
  :type 'float
  :group 'tuareg)

(defcustom ocaml-proccess-name "rlwrap ocaml"
  "Name of the process run by the command ocaml wich run the ocaml toplevel process"
  :type 'string
  :group 'tuareg)

(defun automatic-resize-window (delta)
  "Set the current window size to delta, where delta is the number of lines"
  (if (< (window-body-height) delta)
      (shrink-window (- (window-body-height) delta))
    (enlarge-window (- delta (window-body-height)))))

(defun ocaml ()
  "Execute the current file in the ocaml toplevel process in other window"
  (interactive)
  (save-buffer)
  (set (make-local-variable 'split-height-threshold) 0)
  (let ((current-buffer-name (file-name-nondirectory (buffer-file-name))) (maxsize (window-body-height)))
    (switch-to-buffer-other-window "*ocaml-toplevel*")
    (tuareg-run-process-if-needed ocaml-proccess-name)
    (automatic-resize-window (round (* percentage-window-ocaml-toplevel maxsize)))
    (end-of-buffer)
    (insert (concat "#use \"" current-buffer-name "\";;"))
    (tuareg-interactive-send-input)
    (switch-to-buffer-other-window current-buffer-name)))

;;environnement ocaml
(add-hook 'tuareg-mode-hook
	  (lambda ()
	    (merlin-mode t)		; Load merlin-mode which automatically correct the code
	    (setq highlight-symbol-ignore-list (quote
					   ("if" "then" "else" "match" "with" "fun" "function" "rec" "let" "in" "int" "list" "string" "float" "_"))) ; add some words to not highlight
	    (set (make-local-variable 'compile-command)
		 (concat "ocamlopt " (file-name-nondirectory (buffer-file-name)) " -o " (file-name-sans-extension (buffer-file-name)))) ; Change compile command to ocamlopt
	    (set (make-local-variable 'compilation-read-command) nil) ; Execute compile-command without asking anything
	    (local-set-key (kbd "C-M-i") 'ocaml) ; Execute the file in the ocaml top-level buffer
	    (local-set-key (kbd "M-p")
			    (lambda ()
			      (interactive)
			      (save-buffer)
			      (shell-command (concat compile-command " && xterm -hold -e " (file-name-sans-extension (buffer-file-name)) " &"))
			      ))	; Execute the file in xterm (needed by graphical interfaces)
	    ;; Define how are comments in ocaml
	    (setq-local commenter-config
			'((single .
				  ((comment-start . "(* ")
				   (comment-end . " *)")))
			  (multi .
				 ((comment-start . "(* ")
				  (comment-end . " *)")
				  (comment-continue . " ")
				  (comment-multi-line . t)))))
	    (commenter-setup)		; Set up commenter with the definition abover
	    )
	  t)

