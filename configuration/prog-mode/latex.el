;; Load Latex env
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)


;; Fix some troubleshooting with flyspell
(setq ispell-program-name "aspell")
 ;; Please note ispell-extra-args contains ACTUAL parameters passed to aspell
(setq ispell-extra-args '("--sug-mode=ultra"))
(setq flyspell-issue-mesage-flag nil)	; Fiw lag

;; Set up reftex
(setq reftex-toc-split-windows-horizontally t) ; Reftex always open on the left


;; Set up auto-completion profile for latex
(require 'ac-math) 
(defun my-ac-latex-mode () 
   (setq ac-sources
         (append '(ac-source-yasnippet
		   ac-source-math-unicode
		   ac-source-math-latex
		   ac-source-latex-commands)
                 ac-sources)))


;; Make auctex compatible with flyspell (automatic correction)
(ac-flyspell-workaround) 

;; Some function made on the auctex unit to have fastest compilation command
(require 'tex-buf)
(require 'tex)

(defcustom limit-size-pdf 800000
  "size's limit in octet for the pdf. Avoid lag from emacs"
  :type 'integer
  :group 'LaTeX)

(defun latexmk-command ()
  "Execute commande latexmk on the current file"
  (interactive)
  (save-buffer)
  (TeX-master-file nil nil t)
  (TeX-command "latexmk" 'TeX-master-file))

(defun latex-command ()
  "Execute commande Latex on the current file"
  (interactive)
  (save-buffer)
  (TeX-master-file nil nil t)
  (TeX-command "LaTeX" 'TeX-master-file))

(defun bibtex-command ()
  "Execute commande bibtex on the current file"
  (interactive)
  (save-buffer)
  (TeX-master-file nil nil t)
  (TeX-command "BibTeX" 'TeX-master-file))

(defcustom percentage-latex-window-size 0.4
  "Percentage of the latex window for the pdf window"
  :type 'float
  :group 'LaTeX)

(defun display-buffer-pdf (buffer-name file-path) 
  "display the FILE-PATH in the buffer BUFFER-NAME in a window on the right of the current window.

If the window already exist, the function update the buffer.

The variable percentage-latex-window-size determine the size of the window"
  (interactive)
  (if (windowp (get-buffer-window buffer-name))
      (progn
	(select-window (get-buffer-window buffer-name))
	(revert-buffer nil t)
	(select-window (previous-window)))
    (split-window-right (round (* (- 1 percentage-latex-window-size) (window-body-width))))
    (other-window 1)
    (find-file file-path)
    (select-window (previous-window))))

(defun latex-view ()
  "Open the current pdf if it exists in other window"
  (interactive)
  (let ((output-file-path (concat (file-name-sans-extension (buffer-file-name)) "." (TeX-output-extension)))
	(output-file (concat (file-name-sans-extension (file-name-nondirectory (buffer-file-name))) "." (TeX-output-extension)))
	(buffer-position 'left))
    (if (and (file-exists-p output-file-path)
	     (< (nth 7 (file-attributes output-file-path)) limit-size-pdf))
	(display-buffer-pdf output-file output-file-path)
      (error "Output file %S does not exist or is too big for emacs." output-file)))
  )

;; END of the unit

;; Load my latex configuration
(add-hook 'LaTeX-mode-hook 
	  (lambda()
	    (turn-on-reftex)		; Activate reftex
	    (local-set-key (kbd "C-x t") 'reftex-toc) ; Binding reftex
	    (flyspell-mode 1)			      ; Activate flyspell (automatic correction)
	    (my-ac-latex-mode)			      ; Activate my auto-completion profile
	    (local-set-key (kbd "C-c x") 'TeX-next-error) ; Find error after compilation
	    (local-set-key (kbd "<f5>") 'latexmk-command) ; Run latexmk on the current buffer
	    (local-set-key (kbd "<f6>") 'latex-command)	  ; Run latex command on the current buffer
	    (local-set-key (kbd "C-<f6>") 'bibtex-command) ; Run the bibtex command on the current buffer
	    (local-set-key (kbd "<f7>") 'TeX-view)	   ; Open the pdf in an external viewer
	    (local-set-key (kbd "<f8>") 'latex-view)	   ; Open the pdf in emacs 
	    (push
	     '("latexmk" "latexmk -pdf %s" TeX-run-TeX nil t
	       :help "Run latexmk on file")
	     TeX-command-list)		; Load latexmk command (what ??) TO FIX 
	    (setq ac-math-unicode-in-math-p t) ; display math symbol in emacs
	    )
	  t)


;; Update PDF buffers after successful LaTeX runs
(add-hook 'TeX-after-TeX-LaTeX-command-finished-hook
	  #'TeX-revert-document-buffer)

;; add latexmk as a latex compilation command
(add-hook 'TeX-mode-hook '(lambda () (setq TeX-command-default "latexmk")))
