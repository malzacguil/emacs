;; Add .ino and .pde to arduino mode (always require ??)
(setq auto-mode-alist (cons '("\\.\\(pde\\|ino\\)$" . arduino-mode) auto-mode-alist))

;; Load arduino mode
(autoload 'arduino-mode "arduino-mode" "Arduino editing mode." t)

;; Make changes in arduino mode
(add-hook 'arduino-mode-hook
	  (lambda ()
	    (local-set-key (kbd "M-c") 'arduino-run-arduino) ; Arduino compil command instead of built-in compilator module
	  )t)
