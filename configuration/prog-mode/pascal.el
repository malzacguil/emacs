;; Set up pascal configuration
(add-hook 'pascal-mode-hook
	  (lambda ()
	    (set (make-local-variable 'compile-command)
		 (concat "fpc " (file-name-nondirectory (buffer-file-name)))) ; Change compile command to fpc
	    (set (make-local-variable 'compilation-read-command) nil) ; Don't ask before run compile-command
	    (set (make-local-variable 'highlight-symbol-ignore-list) (quote
					   ("if" "then" "else" "for" "do" "procedure" "function" "begin" "end" "in" "integer" "record" "string" "float"))) ; Add words to not highlight
	    (local-set-key (kbd "M-p")
			    (lambda ()
			      (interactive)
			      (save-buffer)
			      (shell-command (concat compile-command " && xterm -hold -e " (file-name-sans-extension (buffer-file-name)) " &"))
			      ))	; Execute code in xterm (needed by graphical interfaces)
	    (push '(?[ . ?])
		  (getf autopair-extra-pairs :code)) ; Add [] in autopair-mode
	    )
	  t)
