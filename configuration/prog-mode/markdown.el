;; Open a new buffer and display markdown in it
;; Require grip from python
(defun display-markdown ()
  "Display the current markdown code in a buffer"
  (interactive)
  (shell-command "grip &")
  (sleep-for 1)
  (delete-other-windows)
  (split-window-right)
  (other-window 1)
  (xwidget-webkit-browse-url "http://localhost:6419/"))

;; Set up markdown mode
(add-hook 'markdown-mode-hook
	  (lambda ()
	    (local-set-key (kbd "M-c") 'display-markdown) ; Binding my function
	    )
	  t)
