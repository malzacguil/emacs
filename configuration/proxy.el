;; Proxy set up
(setq url-proxy-services
   '(("no_proxy" . "^\\(localhost\\|10.*\\)")
     ("http" . "proxy.pau.eisti.fr:3128")
     ("https" . "proxy.pau.eisti.fr:3128")))

(setq url-http-proxy-basic-auth-storage
    (list (list "proxy.pau.eisti.fr:3128"
                (cons "Input your LDAP UID !"
                      (base64-encode-string "LOGIN:PASSWORD")))))
