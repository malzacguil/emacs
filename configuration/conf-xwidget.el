;; Configure some basic needs to navigate throught xwidget
(add-hook 'xwidget-webkit-mode-hook
	  (lambda ()
	    ;; Map key 
	    (local-set-key (kbd "<down>") 'xwidget-webkit-scroll-up)
	    (local-set-key (kbd "<mouse-5>") 'xwidget-webkit-scroll-up)
	    (local-set-key (kbd "<up>") 'xwidget-webkit-scroll-down)
	    (local-set-key (kbd "<mouse-4>") 'xwidget-webkit-scroll-down)
	    (local-set-key (kbd "<mouse-7>") 'xwidget-webkit-scroll-forward)
	    (local-set-key (kbd "<mouse-6>") 'xwidget-webkit-scroll-backward)
	    (local-set-key (kbd "C-i") 'xwidget-webkit-insert-string)
	    (local-set-key (kbd "<f5>") 'xwidget-webkit-reload)
	    )
	  t)
