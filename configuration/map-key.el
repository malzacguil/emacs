;; multi-curseurs biding keys
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this) ; Add a cursor above
(global-set-key (kbd "C-M->") 'mc/mark-previous-word-like-this) ; Add a cursor on the previous word like the current
(global-set-key (kbd "C->") 'mc/unmark-previous-like-this) ; Delete the previous cursor
(global-set-key (kbd "<C-down-mouse-1>") 'mc/add-cursor-on-click) ; Add/delete a cursor on click


;; Manipulate windows and buffers
(global-set-key (kbd "C-x &") 'delete-other-windows) ; Close all windows except the current one
(global-set-key (kbd "C-x à") 'delete-window) ; Close only the current window
(global-set-key (kbd "C-x é") 'split-window-below) ; Split the window in two horizontally
(global-set-key (kbd "C-x \"") 'split-window-right) ; Split the window two vertically
(global-set-key (kbd "M-b") 'switch-to-buffer) ; Switch to buffer (input name)
(global-set-key (kbd "C-x b") 'buffer-menu) ; Display a window with a list of available buffers
(global-set-key (kbd "C-q") 'kill-buffer-and-window) ; Close a window and kill its buffer
(global-set-key (kbd "M-<left>") 'previous-buffer) ; Back to the previous buffer
(global-set-key (kbd "M-<right>") 'next-buffer) ; Go to the next buffer
(global-set-key (kbd "C-!") 'other-window)  ; Focus on the next window (clockwise)
(global-set-key (kbd "C--") 'shrink-window)  ; Shrink a window vertically
(global-set-key (kbd "C-=") 'enlarge-window) ; Enlarge a window vertically
(global-set-key (kbd "C-6") 'shrink-window-horizontally) ; Shrink a window horizontally
(global-set-key (kbd "C-+") 'enlarge-window-horizontally) ; Enlarge a window horizontally

 
;; Manipulate cursor position
(global-set-key (kbd "M-<up>") 'beginning-of-buffer) ; Go to the top of the buffer
(global-set-key (kbd "M-<down>") 'end-of-buffer) ; Got to the end of the buffer
(global-set-key (kbd "C-b") 'back-to-indentation) ; Go to the beginning of the line


;; Create and manipulate marks
;(global-set-key (kbd "M-m") 'set-mark-command)
;(global-set-key (kbd "M-n") 'pop-global-mark)


;; Display special buffers
;; Open a shell
(global-set-key (kbd "M-t") ' (lambda ()
				(interactive)
				(shell "*shell*")))
;; Open emacs config files
(defun conf-emacs ()
  "Open all the emacs configuration"
  (interactive)
  (delete-other-windows)
  (find-file "~/.emacs.d/")
  (split-window-below)
  (split-window-right)
  (other-window 2)
  (split-window-right)
  (other-window 3)
  (find-file "~/.emacs.d/configuration")
  (other-window 1)
  (find-file "~/.emacs.d/configuration/prog-mode")
  (other-window 1)
  (find-file "~/.emacs.d/configuration/snippets")
  (other-window 1)
  )
(global-set-key (kbd "C-x e") 'conf-emacs)
;; Reload emacs configuration
(global-set-key (kbd "C-x C-e") ' (lambda ()
				   (interactive)
				   (load-file "~/.emacs.d/init.el")))

;; Undo 
(global-set-key (kbd "C-u") 'undo) 
(global-set-key (kbd "C-z") 'undo)
;; Redo
(global-set-key (kbd "C-S-Z") 'undo-tree-redo)
;; Display undo-tree
(global-set-key (kbd "C-x u") 'undo-tree-visualize)
(global-set-key (kbd "C-x z") 'undo-tree-visualize)

;; Commande autocompletion
(global-set-key (kbd "M-x") 'smex) 
(global-set-key (kbd "M-X") 'smex-major-mode-commands)


;; Neotree (based on the vim plugin)
(global-set-key (kbd "<f9>") 'neotree-toggle) ; Open/close neotree


;; Helm feature for yank
(global-set-key (kbd "C-x y") 'helm-show-kill-ring) ; Display kill ring in helm buffer


;; Display and manage all the process start
(global-set-key (kbd "C-x p") 'list-processes)


;; Magit map keys
(bind-keys
 :map global-map
 :prefix '"C-n"
 :prefix-map magit-map
 ("s" . magit-status)
 ("p" . magit-push)
 ("u" . magit-pull)
 ("c e" . magit-checkout)
 ("m" . magit-merge)
 ("o" . magit-clone)
 ("c c" . magit-commit)
 ("f" . magit-stage-file)
 ("a" . magit-stage))


;; Evil number (based an the vim plugin)
(global-set-key (kbd "C-c +") 'evil-numbers/inc-at-pt) ; Increment the number by 1
(global-set-key (kbd "C-c -") 'evil-numbers/dec-at-pt) ; Decrement the number by 1

;; Transparency
(global-set-key (kbd "C-(") 'transparency-decrease)
(global-set-key (kbd "C-)") 'transparency-increase)

;; Other
(global-set-key (kbd "C-r") 'replace-string) ; Replace all the occurences of an expression (beellow the cursor only)
(global-set-key (kbd "M-c") 'compile) ; Buil-in compile command
(global-set-key (kbd "C-t") 'transpose-lines) ; switch the current line with the line above
(global-set-key (kbd "C-a") 'mark-whole-buffer) ; Select all the buffer
