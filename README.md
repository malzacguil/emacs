# Dépôt déplacé !!

5 ans à l'EISTI... Enfin fini ! Ce dépot n'est plus mis à jour depuis longtemps, si jamais quelqu'un tombe dessus et est interessé pour configurer son emacs, j'ai refait un dépôt publique [ici](https://git.malzac.eu/teapot/emacs).

Je remettrais (à jour) la configuration qui permet de passer le proxy. N'hésitez pas à ouvrir une issue au besoin :)


# My emacs configuration

This repository contains all my emacs configuration. You can easily use it, just follow the next intructions.

Be aware that I am french. So spellchecking is set up in french and most of my shortcut are designed for an azerty keyboard.

## Dependencies

* Spellchecking :

  * **aspell**
  * **aspell-french**

* Pdf-tool *(debian-like : to take -dev version)*:

  * **libpng**
  * **zlib1g**
  * **libpoppler-glib**
  * **libpoppler-private**
  * **imagemagick**
  * **poppler**
  * **automake**

* Ocaml *(deactivate by default)*:

  * **opam**
  
      * tuareg
	  * merlin
	  * ocp-indent
	
* xwidgets : 

  * emacs 24+ self-compiled with the option --with-xwidgets

## All-in-one

Here some all-in-one commands to install all dependencies on the OS I already use.

### Arch

```bash
# As root
pacman -S libpng zlib poppler poppler-{glib,data} imagemagick automake opam aspell aspell-fr
# As user
opam init
opam install tuareg merlin ocp-indent
```

### Debian/Ubuntu

```bash
# As root
apt install libpng16-dev zliblg-dev libpoppler-glib-dev libpoppler-private-dev poppler-data poppler-utils imagemagick automake aspell aspell-fr opam
# As user
opam init
opam install tuareg merlin ocp-indent
```

## Installation
 
### Modules

If you want to deactivate or activate some module wich require dependencies follow the next instructions.

#### Deactivate spellchecking

Comment the lines `(setq ispell-program-name "aspell")` and `(setq ispell-dictionary "french")` in the file *configuration/global-configuration.el*.

Comment also the lines `(setq ispell-program-name "aspell")`, `(setq ispell-extra-args '("--sug-mode=ultra"))`, `(setq flyspell-issue-mesage-flag nil)`, `(ac-flyspell-workaround)` and `(flyspell-mode 1)` in the file *configuration/prog-mode/latex.el*

Otherwise, you can set up another languages by install it and change the language in the file *configuration/global-configuration.el* (`(setq ispell-dictionary "french")`).


#### Deactivate pdf-tool 

Comment the line `(pdf-tools-install)` in the file *configuration/global-configuration.el*

#### Activate ocaml

Uncomment the line `(load-file "~/.emacs.d/configuration/prog-mode/ocaml.el")` in the file *configuration/header.el*

#### Deactivate xwidgets

Comment the line `(load-file "~/.emacs.d/configuration/conf-xwidget.el")` in the file *configuration/header.el*

### Requirement

Make sure you haven't configure emacs yet.

```bash
mv ~/.emacs .emacs.old
mv ~/.emacs.d/ .emacs.d.old/
```

### Emacs packages

Move this repository to *~/.emacs.d*

```bash
mv /path/to/emacs ~/.emacs.d/
```

Run emacs. It will load all the needed packages from melpa and elpa, so be patient and take a coffe ! ;)

Installation will be finish when emacs will be maximise and grey. 
